// on commence par initialiser un compteur
		let nPage = 1;
// on initialise aussi les references pour l'image de la baniére, le bouton gauche, le bouton droitet le texte
		const image= document.querySelector(".banner-img");
		const cg = document.querySelectorAll(".arrow_left");
		const titre = document.querySelectorAll(".titre");
		const cd = document.querySelectorAll(".arrow_right");
// on ce créer un petit tableau histoire d'associer nos images a nos textes
		const tableau =
		[
			{
				"image":"slide1.jpg",
				"tagLine":"Impressions tous formats <span>en boutique et en ligne</span>"
			},
			{
				"image":"slide2.jpg",
				"tagLine":"Tirages haute définition grand format <span>pour vos bureaux et events</span>"
			},
			{
				"image":"slide3.jpg",
				"tagLine":"Grand choix de couleurs <span>de CMJN aux pantones</span>"
			},
			{
				"image":"slide4.png",
				"tagLine":"Autocollants <span>avec découpe laser sur mesure</span>"
			}
		]


// c est partie pour la boucle de la fleche de gauche
		cg.forEach(element =>
		{
			element.addEventListener("click", () =>
			{
// on affiche un log pour verifier la bonne prise en compte du clic et on est partie pour baisser de 1 notre compteur a chaque clic
			console.log("Le bouton gauche a été cliqué !");
			nPage=nPage-1;


			if (nPage<=0)
			{
// comme on veux un boucle, si notre conteur est plus petit que 1 on reviens sur la page 4
			nPage=4;
// On change la source de l'image
			const newImageSrc = `./assets/images/slideshow/slide${nPage}.png`;
			image.src = newImageSrc;
// changement de titre
			const imageActuel = tableau[nPage-1].tagLine;
//image actuel prend la bonne tagline
			for (const elem of titre)
			{

			elem.innerHTML = imageActuel;
//parcourt chaque élément de la collection titre puis met à jour le HTML de chaque élément en remplaçant son contenu par la valeur contenue dans imageActuel.
			}
			}
// le reste du code c est la meme
			else if (nPage<=1)
			{
			nPage=1;
			const newImageSrc = `./assets/images/slideshow/slide${nPage}.jpg`;
			image.src = newImageSrc;
			const imageActuel = tableau[nPage-1].tagLine;
			for (const elem of titre) {
			elem.innerHTML = imageActuel;
			}
			}


			else
			{
			const newImageSrc = `./assets/images/slideshow/slide${nPage}.jpg`;
			image.src = newImageSrc;
			const imageActuel = tableau[nPage-1].tagLine;

			for (const elem of titre) {
			elem.innerHTML = imageActuel;
			}
			}
			});
			console.log(nPage);
		});


		cd.forEach(element =>
		{
			element.addEventListener("click", () =>
			{
			console.log("Le bouton de droite a été cliqué !");
			nPage=nPage+1;


			if (nPage>=5)
			{
			nPage=1;
			const newImageSrc = `./assets/images/slideshow/slide${nPage}.jpg`;
			image.src = newImageSrc;
// 			changement de titre
			const imageActuel = tableau[nPage-1].tagLine; //image actuel prend la bonne tagline
			for (const elem of titre) {
			elem.innerHTML = imageActuel;
			}// on change le titre via une boucle
			}

			else if (nPage===4)
			{

			const newImageSrc = `./assets/images/slideshow/slide${nPage}.png`;
			image.src = newImageSrc;
// 			changement de titre
			const imageActuel = tableau[nPage-1].tagLine; //image actuel prend la bonne tagline
			for (const elem of titre) {
			elem.innerHTML = imageActuel;
			}// on change le titre via une boucle
			}


			else if (nPage<=1)
			{
			nPage=1;
			const newImageSrc = `./assets/images/slideshow/slide${nPage}.jpg`;
			image.src = newImageSrc;
// 			changement de titre
			const imageActuel = tableau[nPage-1].tagLine; //image actuel prend la bonne tagline
			for (const elem of titre) {
			elem.innerHTML = imageActuel;
			}// on change le titre via une boucle
			}


			else
			{
			const newImageSrc = `./assets/images/slideshow/slide${nPage}.jpg`;
			image.src = newImageSrc;
// 			changement de titre
			const imageActuel = tableau[nPage-1].tagLine; //image actuel prend la bonne tagline
			for (const elem of titre) {
			elem.innerHTML = imageActuel;
			}// on change le titre via une boucle
			}
			});

		});

